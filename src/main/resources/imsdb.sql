CREATE TABLE "Product" (
  "Product_ID" INT,
  "Name" VARCHAR(200),
  "Product_type" VARCHAR(200),
  "Price" VARCHAR(200),
  PRIMARY KEY ("Product_ID")
);

CREATE TABLE "Customer" (
  "Customer_ID" INT,
  "Email" VARCHAR(200),
  "Login" VARCHAR(200),
  "Password" VARCHAR(200),
  "Contact_number" VARCHAR(200),
  PRIMARY KEY ("Customer_ID")
);

CREATE TABLE "Order" (
  "Order_ID" INT,
  "Customer_ID" INT,
  "Ordered_Date" DATE,
  PRIMARY KEY ("Order_ID"),
  CONSTRAINT "FK_Order.Customer_ID"
    FOREIGN KEY ("Customer_ID")
      REFERENCES "Customer"("Customer_ID")
);

CREATE TABLE "Purchase" (
  "Purchase_ID" INT,
  "Product_ID" INT,
  "Order_ID" INT,
  "Quantity" INT,
  PRIMARY KEY ("Purchase_ID"),
  CONSTRAINT "FK_Purchase.Order_ID"
    FOREIGN KEY ("Order_ID")
      REFERENCES "Order"("Order_ID"),
  CONSTRAINT "FK_Purchase.Product_ID"
    FOREIGN KEY ("Product_ID")
      REFERENCES "Product"("Product_ID")
);

INSERT INTO public."Customer" (
"Customer_ID", "Email", "Login", "Password", "Contact_number") VALUES (
'1'::integer, 'email'::character varying, 'admin'::character varying, 'admin'::character varying, '132456789'::character varying)
 returning "Customer_ID";

  INSERT INTO public."Order" (
"Order_ID", "Customer_ID", "Ordered_Date") VALUES (
'1'::integer, '1'::integer, '22.04.2022'::date)
 returning "Order_ID";

 INSERT INTO public."Product" (
"Product_ID", "Name", "Product_type", "Price") VALUES (
'1'::integer, 'Bike'::character varying, 'Vehicle'::character varying, '100'::character varying)
 returning "Product_ID";

  INSERT INTO public."Purchase" (
"Purchase_ID", "Product_ID", "Order_ID", "Quantity") VALUES (
'1'::integer, '1'::integer, '1'::integer, '1'::integer)
 returning "Purchase_ID";