package pl.borek.ims.dao;

import pl.borek.ims.dao.util.DAOException;
import pl.borek.ims.dao.util.DataAccessObject;
import pl.borek.ims.entity.Purchase;

import java.util.List;
import java.util.Optional;

public class PurchaseDAO extends DataAccessObject<Purchase> {
    @Override
    public Optional<Purchase> findById(int id) throws DAOException {
        return Optional.empty();
    }

    @Override
    public List<Purchase> findAll() throws DAOException {
        return null;
    }

    @Override
    public Purchase update(Purchase dto) throws DAOException {
        return null;
    }

    @Override
    public Purchase create(Purchase dto) throws DAOException {
        return null;
    }

    @Override
    public void delete(int id) throws DAOException {

    }
}
