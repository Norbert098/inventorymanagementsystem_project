package pl.borek.ims.dao;

import pl.borek.ims.dao.util.DAOException;
import pl.borek.ims.dao.util.DataAccessObject;
import pl.borek.ims.entity.Order;

import java.util.List;
import java.util.Optional;

public class OrderDAO extends DataAccessObject<Order> {
    @Override
    public Optional<Order> findById(int id) throws DAOException {
        return Optional.empty();
    }

    @Override
    public List<Order> findAll() throws DAOException {
        return null;
    }

    @Override
    public Order update(Order dto) throws DAOException {
        return null;
    }

    @Override
    public Order create(Order dto) throws DAOException {
        return null;
    }

    @Override
    public void delete(int id) throws DAOException {

    }
}
