package pl.borek.ims.dao;

import pl.borek.ims.dao.util.DAOException;
import pl.borek.ims.dao.util.DataAccessObject;
import pl.borek.ims.entity.Product;

import java.util.List;
import java.util.Optional;

public class ProductDAO extends DataAccessObject<Product> {
    @Override
    public Optional<Product> findById(int id) throws DAOException {
        return Optional.empty();
    }

    @Override
    public List<Product> findAll() throws DAOException {
        return null;
    }

    @Override
    public Product update(Product dto) throws DAOException {
        return null;
    }

    @Override
    public Product create(Product dto) throws DAOException {
        return null;
    }

    @Override
    public void delete(int id) throws DAOException {

    }
}
