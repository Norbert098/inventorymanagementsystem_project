package pl.borek.ims.dao.util;

import java.sql.SQLException;

public class DAOException extends Exception {
    public DAOException(String massage) {
        super(massage);
    }

    public DAOException(String massage, SQLException sqlException) {
        super(massage, sqlException);
    }
}
