package pl.borek.ims.dao;

import pl.borek.ims.dao.util.DAOException;
import pl.borek.ims.dao.util.DataAccessObject;
import pl.borek.ims.entity.Customer;

import java.util.List;
import java.util.Optional;

public class CustomerDAO extends DataAccessObject<Customer> {
    private static final String INSERT = "INSERT INTO Customer(email, login, password, contactNumber) VALUES (?, ?, ?, ?)";
    private static final String FIND_BY_ID = "SELECT Customer(name) VALUES (?)";
    private static final String FIND_ALL = "SELECT Customer(name) VALUES (?)";
    private static final String UPDATE = "SELECT Customer(name) VALUES (?)";
    private static final String DELETE = "SELECT Customer(name) VALUES (?)";

    @Override
    public Optional<Customer> findById(int id) throws DAOException {
        return Optional.empty();
    }

    @Override
    public List<Customer> findAll() throws DAOException {
        return null;
    }

    @Override
    public Customer update(Customer dto) throws DAOException {
        return null;
    }

    @Override
    public Customer create(Customer dto) throws DAOException {
        return null;
    }

    @Override
    public void delete(int id) throws DAOException {

    }
}
