package pl.borek.ims;

import pl.borek.ims.connection.DBConnection;

import static pl.borek.ims.connection.DBConnection.getConnection;

public class Main {
    public static void main(String[] args) {
        getConnection();
    }
}
