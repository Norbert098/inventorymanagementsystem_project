package pl.borek.ims.entity;

import pl.borek.ims.dao.util.DataTransferObject;

public class Purchase implements DataTransferObject {
    private int id;
    private int product_ID;
    private int order_ID;
    private String quantity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "id=" + id +
                ", Quantity='" + quantity + '\'' +
                '}';
    }
}
