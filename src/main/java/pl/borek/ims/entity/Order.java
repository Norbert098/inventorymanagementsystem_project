package pl.borek.ims.entity;

import pl.borek.ims.dao.util.DataTransferObject;

public class Order implements DataTransferObject {
    private int id;
    private int customer_ID;
    private String orderedDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderedDate() {
        return orderedDate;
    }

    public void setOrderedDate(String orderedDate) {
        this.orderedDate = orderedDate;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderedDate='" + orderedDate + '\'' +
                '}';
    }
}
