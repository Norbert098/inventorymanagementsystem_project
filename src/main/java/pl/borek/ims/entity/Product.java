package pl.borek.ims.entity;

import pl.borek.ims.dao.util.DataTransferObject;

public class Product implements DataTransferObject {
    private int id;
    private String name;
    private String product_Type;
    private String price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct_Type() {
        return product_Type;
    }

    public void setProduct_Type(String product_Type) {
        this.product_Type = product_Type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", product_Type='" + product_Type + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
