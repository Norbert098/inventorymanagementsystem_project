package pl.borek.ims.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {
    private static final Logger LOGGER = Logger.getLogger( DBConnection.class.getName());
    protected static String URL_KEY = "url";
    protected static String NAME_KEY = "username";
    protected static String PASSWORD_KEY = "password";


    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    PropertiesEntity.get(URL_KEY),
                    PropertiesEntity.get(NAME_KEY),
                    PropertiesEntity.get(PASSWORD_KEY));
            LOGGER.log(Level.INFO, "---> CONNECTED TO DB <---" );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}
