package pl.borek.ims.connection;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesEntity {
    private static final Properties PROPERTIES = new Properties();
    static {
        loadProperties();
    }

    public PropertiesEntity() {

    }

    public static String get(String key) {
        return PROPERTIES.getProperty(key);
    }

    private static void loadProperties() {
        try (InputStream inputStream = PropertiesEntity.class.getClassLoader().getResourceAsStream("db.properties");) {
            PROPERTIES.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}
